# My learnings notes



# References

Common `application.properties` properties:
https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html

# Steps

On `spring.io` select:

- DevTools
- Security
- Web
- JPA
- JDBC
- PostgreSQL
- DevTools
- H2
- Flyway

Create `.gitignore` at gitignore.io:
https://www.gitignore.io/api/java,macos,linux,windows,eclipse,java-web,intellij+all,gradle

Create `.gitlab-ci.yml` file with the following content:
````yaml
# This file is a template, and might need editing before it works on your project.
# This is the Gradle build system for JVM applications
# https://gradle.org/
# https://github.com/gradle/gradle
image: gradle:alpine

# Disable the Gradle daemon for Continuous Integration servers as correctness
# is usually a priority over speed in CI environments. Using a fresh
# runtime for each build is more reliable since the runtime is completely
# isolated from any previous builds.
variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

build:
  stage: build
  script: gradle --build-cache clean build
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle


test:
  stage: test
  script: gradle check
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle

````

Create the initial packages:
- controller
- model
- repository
- service

Create initial controller:

```java
@RestController
public class HomeController {

    @RequestMapping("/")
    public String home(){
        return "Das boot, returning for duty";
    }
}
```
package com.valtersilva.dasboot.controller;

import com.valtersilva.dasboot.model.Shipwreck;
import com.valtersilva.dasboot.model.ShipwreckStub;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/")
public class ShipwreckController {

    @RequestMapping(value = "shipwrecks", method = RequestMethod.GET)
    public List<Shipwreck> list(){ return ShipwreckStub.list();
    }
}

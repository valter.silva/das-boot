.ONESHELL:

SHELL = /bin/sh

HEADER = "*****"
CONTAINER_NAME := das-boot-db

.PHONY: start-db
start-db: ## Start the postgres database
	docker run -d -p 5432:5432 --name $(CONTAINER_NAME) -e POSTGRES_PASSWORD=postgres -d postgres
	sleep 3

.PHONY: start-generatedata
start-generatedata: ## Start generatedata docker container
	$(info START GENERATEDATA CONTAINERS)
	docker-compose --file docker-compose-generatedata.yml up --detach
	$(info GENERATEDATA AVAILABLE AT http://localhost:8000)

.PHONY: create-db
create-db: ## Creates the database
	docker exec $(CONTAINER_NAME) psql -h localhost -U postgres -c "CREATE DATABASE dasboot;"

.PHONY: connect-db
connect-db: ## Connect to database
	docker exec -it $(CONTAINER_NAME) psql -h localhost -U postgres -d dasboot

.PHONY: stop
stop: ## Stop and remove das-boot container
	-docker stop $(CONTAINER_NAME)

.PHONY: stop-generatedata
stop-generatedata: ## Stop generatedata docker container
	$(warning STOPPING GENERATE DATA)
	-docker-compose --file docker-compose-generatedata.yml stop

.PHONY: destroy
destroy: stop ## Destroy docker containers
	-docker rm $(CONTAINER_NAME)

.PHONY: destroy-generatedata
destroy-generatedata: ## Stop and remove generatedata containers, networks, images, and volumes
	$(warning DESTROYING GENERATE DATA)
	-docker-compose --file docker-compose-generatedata.yml down

.PHONY: destroy-all
destroy-all:
	docker stop $(shell docker ps -aq)
	docker rm $(shell docker ps -aq)

.PHONY: bootstrap
bootstrap: destroy start-db create-db connect-db ## Bootstrap necessary containers


.PHONY: list
list: ## List all docker images and containers
	@echo "***** RUNNING DOCKER CONTAINERS *****"
	docker ps -a
	@echo "***** RUNNING DOCKER IMAGES *****"
	docker images -a

help: ## Show all commands available and their description
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
